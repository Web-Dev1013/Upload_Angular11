import { Component, OnInit } from '@angular/core';
import { VenderService } from './../vender.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  constructor(private venderService: VenderService) {
  }

  ngOnInit(): void {
    this.getListFiles();
  }

  initData: any[] = [];
  useDarkMode: boolean = true;
  selectedFiles?: FileList;
  progressInfos: any[] = [];
  message: string[] = [];
  imageUrl?: string;
  replaceId: any = 0;
  fileInfos: any[] = [];
  image_type : string = "";
  image_name : string = "";
  image_size : string = "";
  baseUrl: string = 'http://localhost:8080/api/files/';

  uploadImage(event: any) {
    this.replaceId = 0;
    this.selectedFiles = event.target.files;
    this.message = [];
    this.progressInfos = [];
    this.uploadFiles();
  }

  replaceImage(event:any){
    this.selectedFiles = event.target.files;
    this.replaceId = event.target.id;
    this.message = [];
    this.progressInfos = [];
    this.uploadFiles();    
  }

  getListFiles(): void {
    this.venderService.getListFiles().subscribe(
      (res: any) => {
      const subThis = this;
        if (res.body != undefined) {
          subThis.initData = [];
          res.body.imageInfos.map(function (item: any, index: any) {
            subThis.initData.push(item);
          });
        }
      },
      (error: any) => {
        console.log(error);
      });
  }
  //uploading selected files
  uploadFiles(): void {
    this.message = [];
    if (this.selectedFiles) {
      for (let i = 0; i < this.selectedFiles.length; i++) {
        this.upload(i, this.selectedFiles[i]);
        this.image_name = this.selectedFiles[i].name;
        this.image_type = this.selectedFiles[i].type;
        this.image_size = this.selectedFiles[i].size + "bytes";
      }
    }
  }

  //upload method
  upload(idx: number, file: File): void {
    this.progressInfos[idx] = { value: 0, fileName: file.name };
    this.imageUrl = '';
    if (file) {
      this.venderService.upload(file, this.replaceId).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            const msg = 'Uploaded the file successfully: ' + file.name;
            this.message.push(msg);
            this.fileInfos.push({ name: file.name, url: this.baseUrl + file.name });
            this.imageUrl = this.imageUrl + file.name + ":";
          }
            this.getListFiles();
        },
        (err: any) => {
          this.progressInfos[idx].value = 0;
          const msg = 'Could not upload the file: ' + file.name;
          this.message.push(msg);
          this.fileInfos.push(file.name);
        });
    }
  }
}
